package dominio;

/**
 * Clase responsable de la información y los atributos de cada producto.
 */
public class Tienda{
	private String producto;
	private int numeroDeProductos;
	private String tienda;

	public Tienda(String producto, int numeroDeProductos, String tienda) {
		this.producto = producto;
		this.numeroDeProductos = numeroDeProductos;
		this.tienda = tienda;
	}

	/**
	 * Establece el nombre del producto.
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}

	/**
	 * Recupera el nombre del producto.
	 */
	public String getProducto() {
		return producto;
	}

	/**
	 * Establece el número de productos enviados.
	 */
	
	public void setNumeroDeProductos(int numeroDeProductos) {
		this.numeroDeProductos = numeroDeProductos;
	}

	/**
	 * Recupera el número de productos enviados.
	 */
	public int getNumeroDeProductos(){
		return numeroDeProductos;
	}
	/**
	 * Establece la tienda a la que se ha enviado 
	*/
	public void setTienda(String tienda) {
	this.tienda = tienda;
	}

	/**
	 * Recupera la tienda a la que se ha enviado.
	 */
	public String getTienda() {
		return tienda;
	}

	/**
	 * devuelve el nombre del producto y el número de productos enviados a las tiendas.
	 */
	public String toString() {
		return getProducto() + " " + getNumeroDeProductos() + " " + getTienda();
	}
	
	/**
	 * sobreescribir el método equals de la clase object"
	 */
	@Override
	public boolean equals(Object obj) {
		Tienda tienda = (Tienda)obj;
		return this.tienda.trim().equals(tienda.getTienda());
	}

}
/**
 * Copyright [2020] [Javier Garc�a Carbia]
 *Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.You may obtain a copy of theLicense 
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.See the License for the specific language governing permissions and limitations under the License.
*/

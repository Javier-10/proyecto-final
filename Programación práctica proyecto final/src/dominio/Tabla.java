package dominio;

import javax.swing.table.TableModel;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList; 
import java.io.IOException;
import java.io.File;
import java.util.Scanner;
import java.io.FileWriter;

/**
 * Clase que se encarga de cada tabla de Tiendas.
 */
public class Tabla{
	private ArrayList<Tienda> listaTiendas = new ArrayList<>();
	private static String nombreFichero = "Tiendas.txt";

	/**
	 * Con este constructor cargamos los contactos que haya almacenados en el fichero.
	 */
	public Tabla(){
		cargarDesdeFichero();
	}

	/**
	 * Añadir un Tienda a la tabla
	 */
	public void annadirTienda(Tienda tienda){
		listaTiendas.add(tienda);
		volcarAFichero();
	}
	public void borrarTienda(Tienda tienda){
		listaTiendas.add(tienda);
		volcarAFichero();
	}
	/**
	 * Mostrar las Tiendas de la tabla
	 */
	public void mostrarTienda(){
		for(Tienda tienda : listaTiendas) System.out.println(tienda);
	}

	private void volcarAFichero(){
		try{
			FileWriter fw = new FileWriter(nombreFichero);
			fw.write(this.toString());
			fw.close();
		}catch(IOException ex){
			System.out.println("Error al intentar escribir en fichero");
		}
	}
	/**
	 * En esta parte cargamos las Tiendas que haya almacenados en el fichero
	 */

	private void cargarDesdeFichero(){
		try{
			File fichero = new File(nombreFichero);
			if (fichero.createNewFile()) {
				System.out.println("Acaba de crearse un nuevo fichero");
			} else {
				Scanner sc = new Scanner(fichero);
				while(sc.hasNext()){
					annadirTienda(new Tienda(sc.next(),Integer.parseInt(sc.next()),(sc.next())));
				}
			}
		}catch(IOException ex){
		}
	}
	@Override
	public String toString (){
		StringBuilder sb = new StringBuilder();
		for(Tienda tienda : listaTiendas) sb.append(tienda + "\n");
		return sb.toString();
	}
}
/**
 * Copyright [2020] [Javier Garc�a Carbia]
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.You may not obtain a copy of the License at
 * http.//www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.See the License for the specific language governing permissions and limitations under the License.
 */

package interfaces;

import dominio.Tabla;
import dominio.Tienda;
import java.lang.ArrayIndexOutOfBoundsException;

/**
 * Creamos una nueva clase llamada Interfaz,y posteriormente se muestra una ayuda de las opciones posibles
 */

public class Interfaz{
	private static Tabla tabla = new Tabla();
	private static void mostrarAyuda(){
		System.out.println("Las instrucciones son las siguientes:");
		System.out.println("Mostrar Tienda: java -jar tabla.jar show");
		System.out.println("Mostrar ayuda: java -jar tabla.jar help");
		System.out.println("Añadir producto: java -jar tabla.jar add <producto> <cantidad> <tienda> ");
		System.out.println("Por ejemplo: java -jar tabla.jar add Patatas 4000 Mercadona");
		System.out.println("Borrar jugador: java -jar tabla.jar rm <producto> <cantidad> <tienda> ");
		System.out.println("Por ejemplo: java -jar tabla.jar rm Patatas 4000 Mercadona");
		System.out.println("Modificar jugador: java -jar tabla.jar modificar <producto> <cantidad> <tienda> <nuevo producto> <nueva cantidad> <nueva tienda> ");
		System.out.println("Por ejemplo: java -jar tabla.jar modificar Patatas 4000 Mercadona Zanahorias 7000 Lidl");
		System.out.println("Generar hoja de cálculo: soffice Distribuidora.csv");
	}

	/**
	 * Creamos un método que ejecute la cadena de parámetros que hemos programado anteriormente
	 */

	public static void ejecutar(String[] args){
		Tabla tabla = new Tabla();
		if (args[0].equalsIgnoreCase("add")){
			tabla.annadirTienda(new Tienda(args[1],Integer.parseInt (args[2]), (args[3])));
		}
		else if (args[0].equalsIgnoreCase("rm")){
			tabla.borrarTienda(new Tienda(args[1],Integer.parseInt (args[2]), (args[3])));
		}
		else if (args[0].equalsIgnoreCase("show")){
			tabla.mostrarTienda();
		}
		else if (args[0].equalsIgnoreCase("help")) mostrarAyuda();
		else if (args[0].equalsIgnoreCase("modificar")){
			tabla.borrarTienda(new Tienda(args[1],Integer.parseInt (args[2]), (args[3])));
			tabla.annadirTienda(new Tienda(args[4],Integer.parseInt (args[5]), (args[6])));

		}else System.out.println("Opción incorrecta, introduzca java -jar tabla.jar help para ayuda");
	}
}
/**
 * Copyright [2020] [Javier García Carbia]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.See the License for the specific language governing permissions and limitations under  License.
 */

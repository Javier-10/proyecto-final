# Notas iniciales

## Compilación del programa

Se ejecuta la siguiente instrucción

~~~~~~~~
make jar
~~~~~~~~

## Uso de la libreta de contactos

Permite ejecutar las siguientes instrucciones:

1. Mostrar productos:

~~~~~~~
java -jar Tabla.jar show
~~~~~~~

2. Mostrar ayuda:

~~~~~
java -jar Tabla.jar help
~~~~~

3. Añadir producto:

~~~~~
java -jar Tabla.jar add <nombre> <cantidad> <tienda>
~~~~~

	por ejemplo,
~~~~~~
java -jar Tabla.jar add Patatas 4000 Mercadona
~~~~~~

4. Borrar producto:
~~~~~
java -jar Tabla.jar rm Patatas 4000 Mercadona
~~~~~

5. Modificar producto:
~~~~~
java -jar Tabla.jar modificar Patatas 4000 Mercadona Zanahorias 7000 Lidl
~~~~~


# Notas extras 

## Generación de Javadoc

Se ejecuta la siguiente instrucción:

~~~~
make javadoc
~~~~

## Inspección de Javadoc

Suponiendo que tiene instalado 'firefox', se ejecuta:

~~~~~
firefox html/index.html
~~~~~

## Sobre el fichero _makefile_

Al haberse utilizado sentencias específicas de Linux, sólo se puede ejecutar en dicho sistema operativo.
